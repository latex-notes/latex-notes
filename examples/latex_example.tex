\documentclass{article}
        
\topmargin 0pt
\advance \topmargin by -\headheight
\advance \topmargin by -\headsep
     
\textheight 8.9in
     
\oddsidemargin 0pt
\evensidemargin \oddsidemargin
\marginparwidth 0.5in
     
\textwidth 6.5in

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}

%%%%%%%%
% Occasionally, you'll need a command that's not recognized by LaTeX.  Use the
%  ``\DeclareMathOperator'' command to define a math operator (provided by the
%  AMs math packages).  Such declarations must be placed in the preamble
%  (between the ``\begin{document}'' below and the ``\usepackage{...}'' above).
%%%%%%%%
\DeclareMathOperator{\supp}{supp}

\begin{document}



We'll start by adding some actual content.
Note that a single return here did not actually cause a line break.

A double space is required for a line break, and signals a new paragraph (hence the indentation).


A triple space ends up being compiled just like a double space.  That is, \LaTeX{} ignores multiple newlines over 2.

You can force a newline with \(\backslash\backslash\), \\
but it won't indent for a new paragraph.

\LaTeX{} provides automatic sizing of headers:

\section{Here's a section}

\subsection{Here's a subsection}

\subsection{Here's another subsection.  Notice \LaTeX{}'s automatic numbering.}

\subsubsection{Here's a subsubsection}

\section*{Here a * was used to suppress the numbering for this section}

\section{Here we have another section.  Notice how the automatic numbering continues.}

\LaTeX{} also provides
\begin{center}
centering justification and
\end{center}
\begin{flushright}
right justification
\end{flushright}
using environments.

We can also make an itemized list:
\begin{itemize}
\item item
\item element
\item thing
    \begin{itemize}
    \item nested item
        \begin{itemize}
        \item doubly nested item
        \item another thing
        \end{itemize}
    \end{itemize}
\item final
\end{itemize}

Or an enumerated list:
\begin{enumerate}
\item first
\item second
    \begin{enumerate}
    \item second first
    \item second second
        \begin{enumerate}
        \item second second first
        \end{enumerate}
    \end{enumerate}
\item third
    \begin{enumerate}
    \item third first
    \end{enumerate}
\end{enumerate}

%%%%%%%%
% Use the ``tabular'' environment to create a table.
% The argument to the ``tabular'' environment consists of some combination of
%  ``l''s, ``c''s, and ``r''s, corresponding to columns that are left, center,
%  and right justified, respectively.  Vertical bars, ``|'', indicate to put a
%  line between adjacent columns.
% The example below creates a 3 column table, with the columns left, right,
%  right, and centered justified.  There is a single vertical line between
%  columns 1 and 2, and a double vertical line between columns 3 and 4.
% The table data is entered by row, with columns delimited by ``&''s and rows
%  delimited by ``\\''s; an ``\hline'' specifies a horizontal line above the
%  next row
%%%%%%%%
Tables are made fairly simply:
\begin{center}
\begin{tabular}{l|rr||c}
\hline
heading1 & heading2 & ... & ... \\
\hline
data & DATA & ... & ... \\
row3 & stuff & ellipses & no ellipses \\
\hline
\end{tabular}
\end{center}

You can also {\em emphasize text}, {\bf bold text}, or {\tt typewrite text}.

\LaTeX{} also has math modes, which provide further commands.  Use inline math mode to include expressions within text:  For \(\epsilon > 0\), choose \(\delta > 0\) such that \(\lvert f(x) - f(y) \rvert < \epsilon\) whenever \(x,y \in D\) with \(\lvert x - y \rvert < \delta\).  Display math mode puts expressions on separate lines and centered:
\[\left\{ x'(t) = f(x(t),t), \ x(0) = x_0 \right\}
 \ \Rightarrow \ x(t) = x_0 + \int_0^t f(x(s),s) ds.\]
Use the {\tt equation} environment to number your displayed equations:
%%%%%%%%
% ``\left'' and ``\right'' must be used in pairs.  They automatically adjust
%  delimiters to be as large as the content between them.  For example, below,
%  the opening and closing parantheses are stretched to be as tall as the 2-row
%  array they enclose.
% Also, the ``array'' environment works almost exactly the same as the
%  ``tabular'' environment.
%%%%%%%%
\begin{equation}
A = \left( \begin{array}{cc} a & b \\ c & d \end{array} \right).
\end{equation}
Actually, a better alternative for matrices is to use the {\tt pmatrix} environment in the AMS math packages:
\[A = \begin{pmatrix} a & b \\ c & d \end{pmatrix}.\]
Some use the {\tt eqnarray} environment to line up a several-line long derivation (note that a ``*'' is used, so this equation won't be given a number):
\begin{eqnarray*}
\det A & = & \left| \begin{array}{cc} a & b \\ c & d \end{array} \right| \\
       & = & ad - bc.
\end{eqnarray*}
Again, the AMs math packages provide a superior alternative to that provided by ``pure'' \LaTeX{}:
\begin{align*}
\det A & = \begin{vmatrix} a & b \\ c & d \end{vmatrix} \\
       & = a d - b c.
\end{align*}
Here's a Taylor Expansion, with numbering:
\begin{align*}
f(x + h) & = f(x) + O(h) \\
         & = f(x) + f'(x) h + O(h^2) \\
         & = f(x) + f'(x) h + \frac{1}{2} f''(x) h^2 + O(h^3) \\
         & = f(x) + f'(x) h + \frac{1}{2} f''(x) h^2 + \frac{1}{6} f^{(3)}(x) h^3 + O(h^4).
\end{align*}
%%%%%%%%
% In the last line above, the braces around ``(3)'' for the superscript command
%  ``^'' were necessary to put all three characters into a superscript; if we
%  had just used ``f^(3)'', then LaTeX would only put the ``('' in the
%  superscript.  This also applies to using the subscript command ``_''.
%%%%%%%%
Some more examples:
\[\supp f = \overline{\left\{ x \in X \ | \ f(x) \neq 0 \right\}}.\]
Most functions \LaTeX{} already has a command for, e.g., \(\sin x\), \(\cos x\), \(\tan x\), \(\sec x\), \(\csc x\), \(\cot x\), \(\log x\), \(\ln x\), \(\sinh x\), \(\cosh x\), \(\tanh x\), \(\exp x\), etc.  Limits are given by
\[\lim_{x \to 0} f(x) \; {\buildrel {\rm ?} \over =} \; f(0).\]
Integrals are given by
\[\int_0^x y dy = \frac{1}{2} x^2,\]
and sums by
\[\sum_{n = 0}^N n = \frac{n(n - 1)}{2}.\]
Page through the ``Short Math Guide for \LaTeX{}'' for additional constructs.

More examples to be added upon request.



\end{document}
