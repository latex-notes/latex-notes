\documentclass[11pt]{article}
\title{Mathematical Logic I\\Homework Assignment IV}
\author{TJ Ellis}
\date{\today}
\usepackage{amssymb,amsmath,amsthm}
\usepackage{fullpage,verbatim,multicol,enumerate}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc} 
\newcommand{\q}[1]{\item \textbf{#1}}
\newcommand{\ans}[1]{#1}
\newcommand{\real}{\ensuremath{\mathbb{R}}}
\newcommand{\nat}{\ensuremath{\mathbb{N}}}
\newcommand{\xz}{\ensuremath{x_{0}}}
\newcommand{\abs}[1]{\ensuremath{\left|#1\right|}}
\newcommand{\norm}[1]{\ensuremath{\left\Vert#1\right\Vert}}
\newcommand{\model}{\ensuremath{\mathcal{M}}}
\newcommand{\val}{\ensuremath{\mathcal{V}}}
\newcommand{\assgn}{\ensuremath{\mathcal{A}}}
\newtheorem{prop}{Proposition}
\newtheorem{thm}{Theorem}
\newtheorem{lem}{Lemma}
\newtheorem{hint}{Hint}
\DeclareMathOperator{\level}{level}
\DeclareMathOperator{\taut}{Taut}
\begin{document}
\maketitle
%\section*{\S1.6}
\begin{comment}
For problem 7, use the compactness theorem for propositional logic or
König's lemma. The key point in each case is to faithfully translate
the given problem into an appropriate set of propositions (or an
appropriate tree). One then applies compactness or König's
lemma. Finally, one must translate the result of this application back
into the terms of the problem.
\end{comment}
\subsection*{\S1.6 \#7.}
A partial order has {\it width at most n} if every set of pairwise
incomparable elements has size at most $n$. A chain in a partial order
$<$ is simply a subset of the order that is linearly ordered by $<$.
\begin{prop}
  An infinite partial order of width at most 3 can be divided into
  three chains (not necessarily disjoint) if every finite partial
  suborder of width at most 3 can be so divided.
\end{prop}
\begin{proof}
  Let $\{p_n\vert n\in\nat\}$ be the set of elements of the infinite
  partial order of width at most 3. Suppose that every finite partial
  suborder of width at most 3 can be divided into three chains. Take
  an arbitrary finite partial suborder of width at most 3, and call
  the three chains into which it can be divided $A$, $B$, and $C$. For
  $i\in\nat$, let propositions $Ap_i$, $Bp_i$ and $Cp_i$ state that
  $p_i$ is in $A$, $B$, or $C$, respectively. For $i,j\in\nat$, let
  $Rp_ip_j$ stand for $p_i<p_j$. Let $p_i=p_j$ iff $i=j$. 
  % Let $I\subset\nat$ be the set of indices of the element of our
  % suborder.
  
  For $A$, $B$, and $C$ to be chains, this means that for any $i\neq
  j\in\nat$, $((Ap_i\wedge Ap_j)\vee(Bp_i\wedge Bp_j)\vee(Cp_i\wedge
  Cp_j))\rightarrow(Rp_ip_j\vee Rp_jp_i)$. For any two elements $p_i,
  p_j$ ($p_i\neq p_j$) to be incomparable, we have $(\neg Rp_ip_j\wedge\neg
  Rp_jp_i)$. Since the suborder has width of at most 3, all sets of
  pairwise incomparable elements has size of at most 3. That is, for
  $i,j,k,h\in\nat$, $(\neg Rp_ip_j\wedge\neg Rp_jp_i\wedge\neg
  Rp_ip_k\wedge\neg Rp_kp_i \wedge \neg Rp_ip_h\wedge\neg
  Rp_jp_h\wedge \neg Rp_kp_j\wedge\neg Rp_jp_k\wedge\neg
  Rp_hp_j\wedge\neg Rp_jp_h\wedge\neg Rp_kp_h\wedge\neg
  Rp_hp_k\wedge)\rightarrow (p_i=p_j\vee p_i=p_k\vee p_i=p_h\vee
  p_j=p_k\vee p_j=p_h\vee p_k=p_h)$

  Let $\Sigma$ be the set of propositions just defined.  That is,
  $\Sigma=\{((Ap_i\wedge Ap_j)\vee(Bp_i\wedge Bp_j)\vee(Cp_i\wedge
  Cp_j))\rightarrow(Rp_ip_j\vee Rp_jp_i)\vert i,j\in \nat; i\neq
  j\}\cup\{(\neg Rp_ip_j\wedge\neg Rp_jp_i\wedge\neg Rp_ip_k\wedge\neg
  Rp_kp_i \wedge \neg Rp_ip_h\wedge\neg Rp_jp_h\wedge \neg
  Rp_kp_j\wedge\neg Rp_jp_k\wedge\neg Rp_hp_j\wedge\neg
  Rp_jp_h\wedge\neg Rp_kp_h\wedge\neg Rp_hp_k\wedge)\rightarrow
  (p_i=p_j\vee p_i=p_k\vee p_i=p_h\vee p_j=p_k\vee p_j=p_h\vee
  p_k=p_h)\vert i,j,k,h\in \nat\}$.  

  Take $\Sigma_I\subset\Sigma$; let $I$ denote the set of indices of
  elements used in $\Sigma_I$. Then take a finite partial suborder of
  width at most 3 with elements $\{p_i\vert i\in I\cup J\}$ (where $J$
  is a set of indices of additional elements needed to make a finite
  partial suborder of width at most 3; $J$ is potentially empty).

  By assumption, this can be divided into 3 chains.  Thus for any
  $i,j\in I\cup J$, $((Ap_i\wedge Ap_j)\vee(Bp_i\wedge
  Bp_j)\vee(Cp_i\wedge Cp_j))\rightarrow(Rp_ip_j\vee Rp_jp_i)$ is
  true, and since it has width at most 3, for any $i,j,k,h\in I\cup
  J$, $(\neg Rp_ip_j\wedge\neg Rp_jp_i\wedge\neg Rp_ip_k\wedge\neg
  Rp_kp_i \wedge \neg Rp_ip_h\wedge\neg Rp_jp_h\wedge \neg
  Rp_kp_j\wedge\neg Rp_jp_k\wedge\neg Rp_hp_j\wedge\neg
  Rp_jp_h\wedge\neg Rp_kp_h\wedge\neg Rp_hp_k\wedge)\rightarrow
  (p_i=p_j\vee p_i=p_k\vee p_i=p_h\vee p_j=p_k\vee p_j=p_h\vee
  p_k=p_h)$ is true.

  Thus any propositions in $\Sigma$ involving only elements with
  indices in $I$ are satisfiable. Thus $\Sigma_I$ is
  satisfiable. Since $\Sigma_I$ was an arbitrary finite subset of
  $\Sigma$, all finite subsets of $\Sigma$ are satisfiable, and thus
  by compactness, $\Sigma$ is satisfiable.

  Since $\Sigma$ is satisfiable, we know that for any $i\neq
  j\in\nat$, $((Ap_i\wedge Ap_j)\vee(Bp_i\wedge Bp_j)\vee(Cp_i\wedge
  Cp_j))\rightarrow(Rp_ip_j\vee Rp_jp_i)$ and that for
  $i,j,k,h\in\nat$, $(\neg Rp_ip_j\wedge\neg Rp_jp_i\wedge\neg
  Rp_ip_k\wedge\neg Rp_kp_i \wedge \neg Rp_ip_h\wedge\neg
  Rp_jp_h\wedge \neg Rp_kp_j\wedge\neg Rp_jp_k\wedge\neg
  Rp_hp_j\wedge\neg Rp_jp_h\wedge\neg Rp_kp_h\wedge\neg
  Rp_hp_k\wedge)\rightarrow (p_i=p_j\vee p_i=p_k\vee p_i=p_h\vee
  p_j=p_k\vee p_j=p_h\vee p_k=p_h)$. That is, we can take the infinite
  partial order consisting of the set of elements $\{p_i\vert
  i\in\nat\}$, and it will be of width at most three, and can be
  divided into three chains.

 
  \begin{comment}
    Suppose there are 4 elements that are pairwise incomparable.  Let
    them be $p_i,p_j,p_k,p_h$. Then we have $(\neg Rp_ip_j\wedge\neg
    Rp_jp_i\wedge\neg Rp_ip_k\wedge\neg Rp_kp_i \wedge \neg
    Rp_ip_h\wedge\neg Rp_jp_h\wedge \neg Rp_kp_j\wedge\neg
    Rp_jp_k\wege\neg Rp_hp_j\wedge\neg Rp_jp_h\wedge\neg
    Rp_kp_h\wedge\neg Rp_hp_k\wedge)$. By the contrapositive of the
    chain propositions, we have $(\neg Rp_ip_j\wedge\neg
    Rp_jp_i\wedge\neg Rp_ip_k\wedge\neg Rp_kp_i \wedge \neg
    Rp_ip_h\wedge\neg Rp_jp_h\wedge \neg Rp_kp_j\wedge\neg
    Rp_jp_k\wege\neg Rp_hp_j\wedge\neg Rp_jp_h\wedge\neg
    Rp_kp_h\wedge\neg Rp_hp_k\wedge)\rightarrow ((\neg Ap_i\vee \neg
    Ap_j)\wedge(\neg Bp_i\vee\neg Bp_j)\wedge(\neg Cp_i\vee\neg
    Cp_j))$
\end{comment}
  
  
  
  
\end{proof}

\begin{comment}
  \paragraph{Hint:}(using compactness) Let the elements of the order be
  $\{p_n\vert n\in\nat\}$. Consider propositions $Rp_ip_j$, $Ap_i$,
  $Bp_i$ and $Cp_i$ for $i,j\in\nat$. Think of $Rp_ip_j$ as saying that
  $p_i<p_j$. Think of $Ap_i$ as saying that $p_i$ is in chain $A$ and
  similarly for $Bp_i$ and $Cp_i$. Now write down the {\it sets} of
  propositions expressing the desired conclusions: Each of $A$, $B$, and
  $C$ is a chain; every element is in $A$, $B$, or $C$; the order has
  width 3.
  \paragraph{Note:} Dilworth's theorem states that any partial order of
  width at most $n$ can be divided into $n$ chains. Thus Dilworth's
  theorem for infinite orders follows from the theorem for finite orders
  by compactness. As the finite case is proved by induction on the size
  of the given order, this is a nontrivial application of compactness.
\end{comment}
\end{document}
