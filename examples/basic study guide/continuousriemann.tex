Our goal for this section is to show that if $f:[a,b]\to\mathbb{R}$
is continuous, then $f$ is Riemann integrable. Recall such
a function is said to be \textbf{Riemann integrable} on $[a,b]$ if
and only if $f$ is bounded and
\[
\forall\epsilon>0,\exists P=\{a=a_{0}<a_{1}<...<a_{n}=b\}\mbox{ s.t. }U(f,P)-L(f,P)<\epsilon
\]
where 
\[
U(f,P)=\sum_{i=0}^{n-1}\sup_{x\in[a_{i},a_{i+1}]}(f(x))(a_{i+1}-a_{i})
\]
 and
\[
L(f,P)=\sum_{i=0}^{n-1}\inf_{x\in[a_{i},a_{i+1}]}(f(x))(a_{i+1}-a_{i}).
\]
 Given $a\leq c<d\leq b$, let 
\[
\Delta(f,c,d)=\sup_{x\in[c,d]}(f(x))-\inf_{x\in[c,d]}(f(x))
\]
 so
\[
U(f,P)-L(f,P)=\sum_{i=0}^{n-1}\Delta(f,a_{i},a_{i+1})(a_{i+1}-a_{i}).
\]


Our proof will make use of uniform continuity, so we develop that
first.


\begin{defn} Let $(X,d)$ and $(Y,\rho)$ be metric spaces.
A function $f:X\to Y$ is \textbf{uniformly continuous} if and only
if 
\[
\forall\epsilon>0,\exists\delta>0\mbox{ s.t. }\forall x,y\in X,\, d(x,y)<\delta\implies\rho(f(x),f(y))<\epsilon.
\]
\end{defn}

The key idea is that if X is a compact space, then any continuous
function from $X$ to a metric space is uniformly continuous.


\textbf{Lemma. }\emph{Let $(X,d)$ be a compact metric space and let
$(Y,\rho)$ be a metric space. Let $f:(X,d)\to(Y,\rho)$ be continuous.
Then, $f$ is uniformly continuous.}


\textbf{Proof of Lemma.} Let $\epsilon>0$. For $x\in X$, let $\delta_{x}>0$
be such that $\forall x,y\in X,\, d(x,y)<\delta_{x}\implies\rho(f(x),f(y))<\frac{1}{2}\epsilon.$
Such a $\delta_{x}$ exists as $f$ is continuous. Now, for $x\in X$,
let $U_{x}=\{w\in X:d(x,w)<\frac{1}{2}\delta_{x}\}$. Then, 
\[
X=\bigcup_{x\in X}U_{x}
\]
so, as $X$ is compact, we may find $x_{1},...,x_{n}$ such that 
\[
X=\bigcup_{i=1}^{n}U_{x_{i}}.
\]
Let $\delta=\frac{1}{2}\min\{\delta_{x_{1}},...,\delta_{x_{n}}\}$.
Then, for $x,y\in X$ with $d(x,y)<\delta,$ we can find an $x_{i}$
with $d(x,x_{i})<\frac{1}{2}\delta_{x_{i}}$as $x\in U_{x_{i}}$ for
some $x_{i}$. Thus, $d(y,x_{i})\leq d(y,x)+d(x,x_{i})<\delta+\frac{1}{2}\delta_{x_{i}}\leq\delta_{x_{i}}$
by our choice of $\delta$, so $\rho(f(x),f(y))\leq\rho(f(x),f(x_{i}))+\rho(f(x_{i}),f(y))<\frac{1}{2}\epsilon+\frac{1}{2}\epsilon=\epsilon.\square$

With this lemma, we are ready to prove our goal.


\begin{thm}Let $a<b$ and suppose $f:[a,b]\to\mathbb{R}$
is continuous. Then, $f$ is Riemann integrable on $[a,b]$.
\end{thm}


\begin{proof}
Let $\epsilon>0.$ As $f$ is continuous and $[a,b]$
is compact, we may apply the lemma and find a $\delta>0$ such that
for all $x,y\in[a,b],\,|x-y|<\delta\implies|f(x)-f(y)|<\frac{\epsilon}{2(b-a)}.$ 

We first show $f$ is bounded on $[a,b]$. Find $m\in\mathbb{N}$
such that $m\delta>(b-a)$. Then, for $x\in[a,b]$, $|x-a|<m\delta$,
so we may find $\{a=x_{0}<x_{1}<x_{2}<...<x_{p}=x\}$ with $p\leq m+1$
such that $(x_{i+1}-x_{i})<\delta$ for all $0\leq i\leq p-1$. Then,
\[
|f(x)|-|f(a)|\leq|f(x)-f(a)|=|\sum_{i=1}^{p-1}(f(x_{i+1})-f(x_{i}))|\leq\sum_{i=1}^{p-1}|f(x_{i+1})-f(x_{i})|<m\frac{\epsilon}{2(b-a)}.
\]
Thus, for $M=|f(a)|+m\frac{\epsilon}{2(b-a)}$, we have $|f(x)|\leq M$
for all $x\in[a,b]$, so $f$ is bounded on $[a,b]$.

Now, let $P=\{a=a_{0}<a_{1}<...<a_{n}=b\}$ be a partition of $[a,b]$
such that for all $0\leq i\leq n-1$, $(a_{i+1}-a_{i})<\delta.$ Then,
$\Delta(f,a_{i},a_{i+1})\leq\frac{\epsilon}{2(b-a)}$, so 
\[
U(f,P)-L(f,P)=\sum_{i=0}^{n-1}\Delta(f,a_{i},a_{i+1})(a_{i+1}-a_{i})\leq\sum_{i=0}^{n-1}\frac{\epsilon}{2(b-a)}(a_{i+1}-a_{i})=\frac{\epsilon}{2}<\epsilon.
\]
\end{proof}

Alternatively, one may prove that the continuous image of a compact
set is compact to conclude that $f$ is bounded, but for this one
needs the fact that if $X\subset\mathbb{R}$ is compact, then $X$
is closed and bounded. While this is not a long proof, it is covered
elsewhere in this guide (cf. the Heine-Borel property).


The proof above generalizes rather readily to higher dimensions, in
which we may replace $[a,b]$ with any compact set $K$.
