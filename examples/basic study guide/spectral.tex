\begin{defn} An operator $T$ on an inner product space $V$
  is called \textbf{normal} if $TT^{*}=T^*T$.
\end{defn}

The Spectral Theorem for finite dimensional inner product spaces states
that given a set of normal linear operators $A_{i}:V\to V$, one can
find a unitary $U:V\to V$ such that $U$ simultaneously diagonalizes
the $A_{i}'s$. This means that all normal operators are simply dilations
along orthonormal bases and, if two normal operators commute, then
they are dilations along the same orthonormal basis. The full mathematical
statement of the theorem is as follows:

\begin{thm}\textbf{The Spectral Theorem over $\mathbb{C}$.}

  Let V be a finite dimensional complex inner product space and let $A_{i}:V\to V$ be a finite set of normal linear operators such that Alg($A_{1},\dots,A_{n},A_{1}^{*},\dots,A_{n}^{*}$) is commutative. Then, there is a unitary $U:V\to V$ and diagonal transformations $D_{1},\dots,D_{n}$ such that for each i, $A_{i}=UD_{i}U^{*}$ is diagonal.
\end{thm}

To prove this, we will use two lemmas.

\begin{lem}\label{spectral_lem1}
  Let $A,B:V\to V$, and $AB=BA$. Let $W=ker(A-\lambda I)$. Then, W is B-invariant.
\end{lem}

\begin{proof}
  Let $w\in W$. Then, $AB(w)=B(A(w))=B(\lambda w)=\lambda B(w)$, so $B(w)\in ker(A-\lambda I)$ as we wished to show.
\end{proof}

\begin{lem}\label{spectral_lem2}
  Let $\{A_{i}:V\to V\}_{i=1}^{n}$be a set of linear maps such that for all $1\leq i,j\le n,A_{i}A_{j}=A_{j}A_{i}$. Then, there is a unitary $U:V\to V$ and upper triangular maps $T_{1},\dots,T_{n}$ such that for each i, $A_{i}=UT_{i}U^{*}$.
\end{lem}

\begin{proof} We proceed by induction on $\dim(V)$. For
  $\dim(V)=1$, we may take $U=\id_{V}$ as all linear maps are upper
  triangular in this case.

  Now assume the result for all complex inner product spaces with
  dimension $m<\dim(V)$. Consider $A_{1}$. By the fundamental theorem
  of algebra, $A_{1}$~has a nonzero eigenvector $v$ with eigenvalue
  $\lambda$. Consider the subspace \[W=\ker(A_{1}-\lambda I).\] We may
  assume $W\neq V,$ as if $W=V$ then $A_{1}=\lambda I$ and for any
  unitary $U:V\to V$, we would have $U\lambda IU^{*}=\lambda
  I$. Moreover, $W\neq\{0\}$ as $v\in W$ and $v\neq0$. Hence, \[1\leq
  \dim(W)<\dim(V).\]
  
  Thus, by the induction hypothesis, there is a unitary $U_{W}:W\to W$
  and upper triangular $T_{1,W},\dots,T_{n,W}:W\to W$ such that for
  all $i$, \[A_{i}|_{W}=U_{W}\cdot T_{i,W}\cdot U_{W}^{*}.\]

  Moreover, $1\leq \dim(W^{\perp})<\dim(V)$, so by the induction
  hypothesis there is a unitary $U_{W^{\perp}}:W^{\perp}\to W^{\perp}$
  and upper triangular
  $T_{1,W{}^{\perp}},\dots,T_{n,W{}^{\perp}}:W{}^{\perp}\to W{}^{\perp}$
  such that for all $i$,
  \[A_{i}|_{W^{\perp}}=U_{W^{\perp}}\cdot T_{i,W^{\perp}}\cdot U_{W^{\perp}}^{*}.\]

  As $W{}^{\perp}$ is the orthogonal complement to $W$, we may extend
  $U_{W},U_{W{}^{\perp}}$ uniquely to a unitary transformation $U:V\to
  V$.  For each $i$, $W$ is an $A_{i}$-invariant subspace by
  lemma \ref{spectral_lem1}, so it follows that $U^{*}A_{i}U$ is upper
  triangular. That is, it has the form: \[\left(\begin{array}{c|c}
    T_{i,W} & *\\ \hline 0 & T_{i,W^{\perp}}
  \end{array}\right)\]
\end{proof}

\begin{proof}[Proof of Spectral Theorem.]
  For $1\leq i\leq2n$, let \[B_{i}=\begin{cases}
  A_{i} & 1\leq i\leq n\\
  A_{j}^{*} & i=n+j,1\leq j\leq n
  \end{cases}\]

  By lemma \ref{spectral_lem2}, there is a unitary $U:V\to V$ and triangular $T_{1},\dots,T_{2n}$
  such that $B_{i}=UT_{i}U^{*}$. For $1\leq i\leq n$, we have \[T_{i}^{*}=(U^{*}B_{i}U)^{*}=U^{*}B_{i}^{*}U=U^{*}B_{i+n}U=T_{i+n}.\]
  but $T_{i}^{*}$ is lower triangular as $T_{i}$ is upper
  triangular. Hence, $T_{i+n}$ is diagonal as it is both upper and lower
  triangular. Thus, $T_{i}$ is diagonal for each \emph{i}, as the adjoint
  of a diagonal transformation is diagonal.

\end{proof}

Note that a similar result holds over $\mathbb{R}$. The proof is
the same, except in this case one needs to show that $A_{1}$ has
a real nonzero eigenvector.

By applying the spectral theorem to a single normal operator, we see
that all normal operators are unitarily diagonalizable.

Checking the commutivity of every pair of elements of $A_{1},\dots,A_{n},A_{1}^{*},\dots,A_{n}^{*}$
is unneccesary: if $A,B$ are normal and $AB=BA$, then $A^{*}B=BA^{*}$.
To see this, we write $A=\sum\lambda_{j}P_{j}$ (with $\lambda_{i}=\lambda_{j}$ if
and only if $i=j$) where $\{P_{j}\}$ is a commuting family of projections
onto the eigenspaces of $A$. Now apply the Spectral Theorem to only $A$.
Then, $A^{m}=\sum\lambda_{j}^{m}P_{j},$ so for 
\[
p(x)=\prod_{i\neq j}(x-\lambda_{i})
\]


we have $P_{j}=p(A)$. Hence, as $B$ commutes with $A$, $B$ commutes
with each $P_{j}.$ Now, $A^{*}=\sum\overline{\lambda_{j}}Pj$, so $A^{*}B=BA^{*}.$
