While notions such as density make sense in general topological settings,
for our purposes it is sufficient to work in a metric space. Hence,
for the following we work in the metric space $(X,d)$ with the usual
metric topology.

\begin{defn}
  A subset $A\subset X$ is \textbf{dense} if and
  only if $cl(A)=\overline{A}=X$.
\end{defn}

In our case, this is equivalent to for every $x\in X$ and every $\epsilon>0$,
$B(x,\epsilon)\bigcap A\neq\emptyset$.

\begin{defn}A subset $A\subset X$ is \textbf{nowhere dense}
  if and only if $\overline{A}^{\circ}=int(\overline{A})=\emptyset$.
\end{defn}

In our case, this means for every $x\in X$ and every $\epsilon>0$,
we can find a $\delta>0$ and an $x'\in B(x,\epsilon)$ such that
$B(x',\delta)\subset B(x,\epsilon)$ and $B(x',\delta)\bigcap A=\emptyset$.
Note that $A\subset X$ is nowhere dense if and only if $\overline{A}$
is nowhere dense.


\begin{defn} A subset $A\subset X$ is of \textbf{first category}
  or \textbf{meager} (also spelled \textbf{meagre}) if and only if there
  are $\{A_{n}\}_{n\in\mathbb{N}}$ such that each $A_{n}$ is nowhere
  dense and $A=\bigcup_{n=1}^{\infty}(A_{n})$.
\end{defn}

It is quick to check that countable unions of meager sets are themselves
meager. Complements of meager sets are called \textbf{residual}. The
counterpart to first category is second category.

\begin{defn} A subset $A\subset X$ is of \textbf{second category}
  if and only if it is not first category.
\end{defn}

Second category sets are thus ``large'' sets in some sense. The
Baire category theorem makes this explicit:

\begin{thm}[Baire Category]
  Let $(X,d)$ be a complete
  metric space. Suppose $\{A_{n}\}_{n\in\mathbb{N}}$ is a sequence
  of nowhere dense subsets of $X$. Then, $X\backslash\bigcup_{n=1}^{\infty}(A_{n})$
  is dense in $X$.}
\end{thm}

In particular, $X$ is not of first category, nor is $X$ a countable
union of first category sets.

\begin{proof}
  Let $\{A_{n}\}_{n\in\mathbb{N}}$ be a collection of nowhere dense
  subsets of $X$. As $\overline{A_{n}}$ is also nowhere dense, it
  suffices to assume all $A_{n}$ are closed.

  Let $A=\bigcup_{n=1}^{\infty}(A_{n})$.  Let $\epsilon>0$ and let
  $x\in X$. We show that \[B(x,\epsilon)\subseteq\bigcap(X\setminus
  A)\neq\emptyset.\]
  
  To do so, we construct a Cauchy sequence $\{y_{n}\}$
  inductively as follows:
  \begin{itemize}
  \item Find $\delta_{1}>0$ and $y_{1}\in
    B(x,\epsilon)$ such that $\overline{B(y_{1},\delta_{1})}\bigcap
    B_{1}=\emptyset$. This is possible as $A_{1}$ is nowhere
    dense.
  \item Given $y_{1},\dots,y_{n}$ and $\delta_{1},\dots,\delta_{n}$,
    find $\delta_{n+1}>0$ and $y_{n+1}\in\overline{B(y_{n},\delta_{n})}$
    such that $\delta_{n+1}<\frac{\delta_{n}}{2}$,
    $\overline{B(y_{n+1},\delta_{n+1})}\subset B(y_{n},\delta_{n})$, and\\
    $\overline{(B(y_{n+1},\delta_{n+1}))}\bigcap A_{n+1}=\varnothing$.
    This is possible as $A_{n+1}\bigcap\overline{(B(y_{n},\delta_{n}))}$
    is nowhere dense.
  \end{itemize}

  Now, for $M\leq N$, we have $y_{M},\,
  y_{N}\in\overline{B(y_{M},\delta_{M})}$, so
  \[d(x_{M},x_{N})\leq\delta_{M}<\frac{\delta_{M-1}}{2}<...<\frac{\delta_{1}}{2^{M}}\]
  and hence $\{y_{n}\}_{n\in\mathbb{N}}$ is a Cauchy
  sequence. Therefore, as $X$ is complete, there is a $y\in X$ such
  that $y=\lim_{n\to\infty}y_{n}$.  As $y_{M},\,
  y_{N}\in\overline{B(y_{M},\delta_{M})}$ whenever $M\leq N$, we have
  $y\in\overline{B(y_{M},\delta_{M})}\subset
  B(y_{M-1},\delta_{M-1})\subset A_{M-1}^{c}$.  As this holds for all
  $M$, we have
  $y\in\bigcap_{n=1}^{\infty}(A_{n})^{c}=(\bigcup_{n=1}^{\infty}(A_{n}))^{c}=X\backslash
  A,$ as we wished to show.
\end{proof}

One of the more common qual questions involving category is to show
that $\mathbb{Q}$ is not the countable intersection of open sets
in $\mathbb{R}$. This is done as follows:


\begin{cor}\emph{$\mathbb{Q}$ is not the countable intersection
    of open sets in $\mathbb{R}$.}
\end{cor}

\begin{proof}
  Suppose we have $\{U_{n}\}_{n\in\mathbb{N}}$ open
  subsets of $\mathbb{R}$ such that
  $\bigcap_{n=1}^{\infty}(U_{n})=\mathbb{Q}$.  Then, for each
  $n\in\mathbb{N},$ $\mathbb{Q}\subset U_{n}$, so each $U_{n}$ is dense
  in $\mathbb{R}$. Thus, $C_{n}=\mathbb{R}\backslash U_{n}$ is a closed
  nowhere dense subset of $\mathbb{R}$ for each $n$. Hence,
  $\bigcup_{n=1}^{\infty}(C_{n})=(\bigcap_{n=1}^{\infty}(U_{n}))^{c}=\mathbb{R}\backslash\mathbb{Q}$
  is meager. But $\mathbb{Q}$ is countable, so
  $\mathbb{Q}=\bigcup_{i=1}^{\infty}\{r_{i}\}$ where
  $\{r_{i}\}_{i=1}^{\infty}$ is an enumeration of $\mathbb{Q}$.  Hence,
  $\mathbb{Q}$ is meager, so
  $\mathbb{Q}\bigcup(\mathbb{R}\backslash\mathbb{Q})$ is meager, but
  this is $\mathbb{R}$ which is nonmeager by the Baire category
  theorem. Hence, $\bigcap_{n=1}^{\infty}(U_{n})=\mathbb{Q}$ is
  impossible.
\end{proof}
