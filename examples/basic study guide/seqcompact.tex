We will show the equivalence of the following three conditions in a metric space $(X,d)$:
\begin{enumerate}
\item $X$ is compact
\item $X$ is sequentially compact
\item $X$ is complete and totally bounded.
\end{enumerate}

\begin{defn}
  A metric space $X$ is \textbf{totally bounded} if for any $\eps>0$, $X$ can be covered with finitely many $\eps$-balls.
\end{defn}

\begin{thm}
  Any compact metric space is sequentially compact
\end{thm}
\begin{proof}
  Consider a sequence $s_1,s_2,\dots$ in a compact metric spcae $(X,d)$, and suppose for the sake of contradiction that no subsequence converges. This means that for each $x\in X$, there is some $\eps_x>0$ such that $B(x,\eps_x)$ contains only finitely many of the $s_i$ (for otherwise they would form a subsequence which converged to $x$. So consider the open cover of $X$, and by compactness, take a finite subcover \[X=\displaystyle\bigcup_{x\in X}B(x,\eps_x)=\bigcup_{j=1}^nB(x_j,\eps_{x_j}).\]

  But each $B(x_i,\eps_{x_j})$ contains only finitely many of the $s_i$. This would mean that there are only finitely many elements in our sequence $s_1,s_2,\dots$. This is clearly a contradiction, so every compact metric space must be sequentially compact.
\end{proof}

\begin{thm}
  Any sequentially compact metric space is complete and totally bounded.
\end{thm}

\begin{proof}
Given a metric space $(X,d)$, consider a Cauchy sequence $x_1,x_2,\dots$. It has a convergent subsequence $\{x_{n_k}\}\to x$. Thus, \[\forall\eps>0\ \exists N_\eps\mbox{ s.t. }k>N_\eps\Longrightarrow d(x,x_{n_k})<\frac\eps2.\]

Furthermore, by Cauchyness, \[\forall\eps>0\ \exists M_\eps\mbox{ s.t. }n,m>N_\eps\Longrightarrow d(x_m,x_n)<\frac\eps2.\]

Then for any $m>\max\{N_\eps,M_\eps\}$, we see that we can pick some $n_k>\max\{N_\eps,M_\eps\}$ such that \[d(x,x_m)\le d(x,x_{n_k})+d(x_{n_k},x_m)\le \eps,\] proving that $X$ is complete.

To see that $X$ is totally bounded, first assume it is not, and take some $\eps>0$ for which $X$ cannot be covered with finitely many $\eps$-balls. Pick a sequence $p_1,p_2,\dots$ by picking each $p_n$ such that
\[p_n\in X\setminus\bigcup_{k=0}^{n-1}B(p_k,\eps)\neq\varnothing.\]

This sequence cannot have a convergent subsequence, since no pair of points $p_i,p_j$ are within $\eps$ of each other. That is, without loss of generality, let $i<j$. Then $p_j$ was chosen specifically such that $p_j\notin B(p_i,\eps)$. This is a contradiction, so $X$ must be totally bounded.
\end{proof}

\begin{lem}
  A totally bounded metric space has a countable base for its topology.
\end{lem}

\begin{proof}
  For each $N$, we can cover $X$ with only finitely many balls of radius $\frac1N$. So each point has arbitrarily small balls contianing it. For each $N\in\N$, if we take such a finite set of balls, the collection of all such balls is a basis for the topology.
\end{proof}

\begin{lem}
  A totally bounded metric space is Lindel\"of (i.e., every open cover has a countable subcover).
\end{lem}

\begin{proof}
  Take a cover $X=\bigcup U_\alpha$ of $X$. For each point $x$, consider an open set $U_\alpha\ni x$ and a basic open set $B_x\ni x$ for which $B_x\subseteq U_\alpha$. It is clear that $\bigcup B_x$ is an open cover of $X$. Since there are only countably many basic open sets, it is a countable open cover. But $B_x\subseteq U_\alpha$, so there must also be a countable subcover of $\bigcup U_\alpha$.
\end{proof}

\begin{thm}
A complete and totally bounded metric space is compact.
\end{thm}

\begin{proof}
Since $X$ is totally bounded, and therefore Lindel\"of, it suffices to show that every countable cover has a finite subcover. Suppose we have a countable open cover of $X$ with no finite subcover. Label the open sets $U_1,U_2,\dots$, and define \[G_n=U_1\cup\dots\cup U_n.\]

If any $G_n=X$, we have produced a finite subcover. Otherwise, we can produce an $r^{(0)}_n\in X\setminus G_n$ for each $n$.

We will define the sequence $\{r^{(k)}_i\}_{i=1}^\infty$ recursively as follows. Assuming we already have defined $\{r^{(n-1)}_i\}_{i=1}^\infty$, cover $X$ with balls of radius $\frac1n$. For instance $\bigcup_{x\in X}B\left(x,\frac1n\right)$ is such a cover. Because $X$ is totally bounded, there must be a finite subcover, meaning there is some ball of radius $\frac1n$ (say, about a point $p_n$) which contains infinitely many of the $r_i^{(n-1)}$. That is, there is a subsequence which is within $\frac1n$ of $p_n$. Pick $\{r^{(n)}_i\}_{i=1}^\infty$ to be such a subsequence.

Now consider the sequence $r^{(1)}_1,r^{(2)}_1,r^{(3)}_1,\dots$. We wish to prove that this sequence is Cauchy. So let $\eps>0$, and pick $N$ such that $N\eps>2$. Now, for any $m,n\ge N$, $r^{(m)}_1,r^{(n)}_1\in\{r_i^{(N)}\}_i=1^\infty$. Thus,
\[d(r^{(m)}_1,r^{(n)}_1)\le d(r^{(m)}_1,p_N)+d(p_N,r^{(n)}_1)\le \frac1N+\frac1N<\eps.\]

Since $r^{(1)}_1,r^{(2)}_1,r^{(3)}_1,\dots$ is Cauchy, and $X$ is complete, it converges to some point $x\in X$. We know that $x$ lies in some open $U_m$. Since $U_m$ is open, there must be infinitely many $k$ for which $r^{(k)}_1\in U_m$. But for no $k>m$ can it be that $r^{(k)}_1\in G_m\supseteq U_m$.

This is a contradiction, so every complete and totally bounded metric space must be compact.
\end{proof}
