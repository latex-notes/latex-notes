We are often interested in not just sequences of continuous functions $f_n$, but also their infinite sum $f(x) = \sum_{n=1}^\infty f_n(x) := \lim_{N \to \infty} \sum_{n=1}^N f_n(x)$. When does the limit $f$ exist, and when is it continuous? The Weierstrass M-Test gives us a useful criterion.

\begin{thm}[Weierstrass M-Test]
Let $f_n: X \to \mathbb{R}$ be a sequence of continuous functions on a  metric space $(X, d)$. Suppose there is a sequence of numbers $M_n \ge 0$ such that $\sup_{x\in X} |f_n(x)| \le M_n$ and $\sum_{n} M_n < \infty$. Then $\sum_{n=1}^\infty f_n$ converges uniformly to the function $f$ defined by $f(x) = \sum_{n=1}^\infty f_n(x)$. In particular the limit $f$ is continuous.
\end{thm}

\begin{proof}
For each $N$, let $S_N(x) = \sum_{n=1}^N f_n(x), \ S'_N = \sum_{n=1}^N M_n$ be the $N$th partial sums. By definition, $\sum_{n=1}^\infty f_n = \lim_{N \to \infty} S_N$. We show that the sequence $\{S_N\}_N$ is uniformly Cauchy. As the $S_N$ are continuous, the completeness of $C(X)$ then implies that $\sum_{n = 1}^\infty f_n$ converges uniformly to the function $f$ defined by the pointwise limit $f(x) = \sum_{n=1}^\infty  f_n(x)$, which is therefore continuous.

As $\sum_{n=1}^\infty M_n$ converges, for each $\varepsilon > 0$, there exists some $N_0$ such that $\sum_{n=N_0}^\infty M_n < \varepsilon$. For all $M > N \ge N_0$, we have 
\begin{align*}
|S_M(x) - S_N(x)| = \left| \sum_{n=N}^{M-1} f_n(x) \right| \le \sum_{n=N}^{M-1} |f_n(x)| \le \sum_{n=N}^{M-1} M_n \le \sum_{n=N_0}^\infty M_n < \varepsilon,
\end{align*}
so $\sup_{x} |S_M(x) - S_N(x)| < \varepsilon$. Thus $\{S_N\}_N$ is uniformly Cauchy as required.
\end{proof}
