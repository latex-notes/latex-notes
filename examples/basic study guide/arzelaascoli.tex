In this note we prove the following theorem:

\begin{thm}[Strong formulation]
  Let $(X,d)$ be a compact metric space and let $(f_n)_{n=1}^\infty$,
  $f_n\colon X\to\mathbb{C}$, be a pointwise bounded and pointwise
  equicontinuous sequence. Then $(f_n)$ has a uniformly convergent
  subsequence with a continuous limit.
\end{thm}

In order to do this, we first prove the following seemingly weaker
version:

\begin{thm}[Weak formulation]
  Let $(X,d)$ be a compact metric space and let $(f_n)_{n=1}^\infty$,
  $f_n\colon X\to\mathbb{C}$, be a pointwise bounded and uniformly
  equicontinuous sequence. Then $(f_n)$ has a uniformly convergent
  subsequence with a continuous limit.
\end{thm}

We need the following lemma:

\begin{lem} Let $(X,d)$ be a compact metric space. Then there exists a countable dense subset of $X$ (so that, by definition, $X$ is a separable topological space).
\end{lem}

\begin{proof} Let $n\in\mathbb{N}$ be given. Then $\{B(x,\frac{1}{n})\,:\,x\in X\}$ is an open covering of $X$.
  Hence there exists by compactness $x_1^n,\ldots,x_{m_n}^n\in X$ such
  that $X = \bigcup_{j=1}^{m_n} B(x_j^n,\frac{1}{n})$.  Put $A =
  \{x_j^n\,:\,n\in\mathbb{N}, j = 1,\ldots,m_n\}$, which is clearly
  countable. We claim that $A$ is dense in $X$. Let therefore $x\in X$
  be given and let $B(x,\epsilon)$ be any open ball around $x$. Choose
  $n\in\mathbb{N}$ with $\frac{1}{n}<\epsilon$. Since $X =
  \bigcup_{j=1}^{m_n} B(x_j^n,\frac{1}{n})$, we deduce that $x\in
  B(x_j^n,\frac{1}{n})$ for some $j$. Then $d(x,x_j^n)<\epsilon$ so
  that $x_j^n\in B(x,\epsilon)\cap A$. This completes the
  proof. \end{proof}

We also recall:

\begin{defn}
  Let $(X,d)$ be a metric space and let for each $n\in\mathbb{N}$ a
  function $f_n\colon X\to \mathbb{C}$ be given.
  \begin{itemize}
  \item The sequence $(f_n)$ is called {\it pointwise equicontinuous}
    if for all $x\in X$ and all $\epsilon > 0$ there exists $\delta >
    0$ such that whenever $d(x,y)<\delta$ for some $y\in X$ we have
    $|f_n(x)-f_n(y)|<\epsilon$ for all $n$.
  \item The sequence $(f_n)$ is called {\it uniformly equicontinuous}
    if for all $\epsilon > 0$ there exists $\delta > 0$ such that
    whenever $d(x,y)<\delta$ for $x,y\in X$ we have
    $|f_n(x)-f_n(y)|<\epsilon$ for all $n$.
  \end{itemize}
\end{defn}

\begin{proof}[Proof of weak formulation.]
  Choose by the lemma a countable dense set $A =
  \{a_1,a_2,\ldots\}$. As $(f_n(a_1))_{n=1}^\infty$ is bounded, and
  hence contained in a compact subset of $\mathbb{R}$, it has a
  convergent subsequence $(f_n^1(a_1))_{n=1}^\infty$. Similarly there
  exists a convergent subsequence $(f_n^2(a_2))_{n=1}^\infty$ of
  $(f_n^1(a_2))_{n=1}^\infty$. Continuing in this way, we obtain for
  each $k$ a subsequence $(f_n^k)_{n=1}^\infty$ of
  $(f_n)_{n=1}^\infty$ converging at $a_1,\ldots,a_k$ such that if
  $l>k$ then $\lim_{n\to\infty} f_n^l(a_k) = \lim_{n\to\infty}
  f_n^k(a_k)$. (This follows from the fact that any subsequence of a
  convergent sequence converges to the same limit.) Denote the common
  limit at $a_k$ by $a_k^*$ and put $g_n = f_n^n$ for each
  $n\in\mathbb{N}$. Note that $(g_n)_{n=1}^\infty$ is a subsequence of
  $(f_n)_{n=1}^\infty$. We claim that $g_n(a_k)\to g(a_k)$ for each
  $k\in\mathbb{N}$. To see this, let $\epsilon > 0$ and
  $k\in\mathbb{N}$ be given. Choose $N\geq k$ such that
  $|f_n^k(a_k)-a_k^*|<\epsilon$ for $n\geq N$. Note that, for $n\geq
  N$, $f_n^n(a_k)$ is an element of $(f_n^k(a_k))_{n=1}^\infty$ whose
  index is at least as large as $N$. Hence $|g_n(a_k)-a_k^*|<\epsilon$
  whenever $n\geq N$, proving the claim.
  
  Let now $\epsilon > 0$ be given. Choose $\delta > 0$ such that
  whenever $d(x,y)<\delta$ for some $x,y\in X$, we get that
  $|f_n(x)-f_n(y)|<\epsilon/3$ for all $n$. Cover $X$ by finitely many
  balls of radius $\delta/2$ and choose one $a_k$ in each of
  these. Choose $N\geq n$ such that $|g_n(a_k)-g_m(a_k)|<\epsilon/3$
  for each of these finitely many chosen $a_k$ whenever $n,m\geq
  N$. Fix such $n,m$. Then, given $y\in X$, choose $a_k$ in the same
  $\delta/2$-ball as $y$. Then $d(y,a_k)\leq \delta$ and hence
  $$ |g_n(y)-g_m(y)|\leq
  |g_n(y)-g_n(a_k)|+|g_n(a_k)-g_m(a_k)|+|g_m(a_k)-g_m(y)| < \epsilon.
  $$ Thus
  $$ \sup_{y\in X}|g_n(y)-g_m(y)| < \epsilon.
  $$ Hence $(g_n)_{n=1}^\infty$ is a Cauchy sequence in the complete
  metric space $C(X)$. It follows that it is uniformly convergent to a
  continuous limit.
\end{proof}

The following lemma will prove the strong formulation.

\begin{lem}
  Let $(X,d)$ be a compact metric space and let $(f_n)_{n=1}^\infty$
  be a pointwise equicontinuous sequence of function
  $X\to\mathbb{C}$. Then $(f_n)_{n=1}^\infty$ is uniformly
  equicontinuous.
\end{lem}

\begin{proof}
  Suppose that $(f_n)_{n=1}^\infty$ were not uniformly
  equicontinuous. Then there exists an $\epsilon_0> 0$ such that for
  all $\delta > 0$ there exist $x,y\in X$ such that $d(x,y)<\delta$
  but $|f_n(x)-f_n(y)|\geq \epsilon_0$ for some
  $n\in\mathbb{N}$. Given $n\in\mathbb{N}$, we can therefore choose
  $x_n,y_n\in X$ such that $d(x_n,y_n)<\frac{1}{n}$ and
  $|f_{m_n}(x_n)-f_{m_n}(y_n)|\geq \epsilon_0$ (with
  $m_n\in\mathbb{N}$).  Since $X$ is compact, $(x_n)_{n=1}^\infty$ has
  a convergent subsequence, say $x_{n_k}\to x\in X$.  Since
  $d(x_{n_k},y_{n_k})<\frac{1}{n_k}$ for all $k\in\mathbb{N}$, it
  follows that $y_{n_k}\to x$ as well.  Let now $\delta > 0$ be
  given. Then $d(x_{n_k},x)<\delta$ and $d(y_{n_k},x)<\delta$
  eventually, but $\epsilon_0\leq
  |f_{m_{n_k}}(x_{n_k})-f_{m_{n_k}}(y_{n_k})| \leq
  |f_{m_{n_k}}(x_{n_k})-f_{m_{n_k}}(x)|+|f_{m_{n_k}}(x)-f_{m_{n_k}}(y_{n_k})|$
  so that either $|f_{m_{n_k}}(x_{n_k})-f_{m_{n_k}}(x)|$ or
  $|f_{m_{n_k}}(x)-f_{m_{n_k}}(y_{n_k})|$ is at least of size
  $\epsilon_0$. Thus the pointwise equicontinuity of
  $(f_n)_{n=1}^\infty$ fails at the point $x$, proving the lemma.
\end{proof}
